FROM node:8.11.3

WORKDIR /myTarget
ADD . /myTarget

RUN yarn install
RUN yarn build

EXPOSE 8080

CMD bash -c "yarn build"